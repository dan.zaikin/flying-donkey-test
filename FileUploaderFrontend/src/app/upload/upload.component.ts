
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { HttpEventType, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  public progress: number;
  public message: string;
  @Input() public user: string;
  @Output() public onUploadFinished = new EventEmitter();

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  public uploadFile = (files) => {
    if (files.length === 0) {
      return;
    }

    let fileToUpload = <File>files[0];
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    formData.append('user', this.user);

    this.http.post('http://localhost:4201/api/fileupload', formData, { reportProgress: true, observe: 'events' })
      .subscribe((event) => {
        if (event.type === HttpEventType.UploadProgress)
          this.progress = Math.round(100 * event.loaded / event.total);
        else if (event.type === HttpEventType.Response) {
          if (event.status != 200) {
            this.message = 'Upload failed: ' + event.status + ' - ' + event.statusText
          } else {
            this.message = 'Upload success.';
            this.onUploadFinished.emit(event.body);
          }
        }
        (error) => {
          this.message = 'Upload failed: ' + error.statusText;
          console.log('Memes error: ' + error.statusText);
        }
      });
  }
}
