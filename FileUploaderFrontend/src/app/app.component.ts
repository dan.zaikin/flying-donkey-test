import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
  public isCreate: boolean;
  public isUpload: boolean;
  public isView: boolean;
  public title: string = "FileUploaderFrontend"
  public userName: string;
  public metadata: any;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.isCreate = true;
  }

  public uploadFinished = (event) => {
    this.isUpload = false;

    
    this.isView = true;
  }

  public returnToCreate = () => {
    this.isCreate = true;
    this.isUpload = false;
    this.isView = false;
  }

  public onSet = () => {
    this.isCreate = false;
    this.isUpload = true;
  }


}
