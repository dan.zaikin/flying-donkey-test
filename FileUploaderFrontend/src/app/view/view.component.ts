import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface FileData {
  User: string;
  FileName: string;
  FileSize: number;
  FileSizeString: string;
  UploadTime: string;
}

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  public metadata: Map<string, FileData[]>;
  public testDictionary: Map<string, number>;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getMetadata();
  }

  public getMetadata() {
    this.http.get('http://localhost:4201/api/fileupload')
      .subscribe((res: any) => {
        console.log(res);
        if (!res.metadataJson) {
          throw Error("No response body found")
        }
        this.metadata = this.formatMetadata(res.metadataJson);
      });
  }

  public formatMetadata(metadataToFormat: Map<string, FileData[]>) {
    let metadataToReturn: Map<string, FileData[]> = new Map<string, FileData[]>();
    let testArray: string[] = [];
    for (let fileType in metadataToFormat) {
      metadataToReturn.set(fileType, []);
      for (let fileData of metadataToFormat[fileType]) {
        fileData.FileName = fileData.FileName + fileType;
        if (fileData.FileSize < 1000000) {
          fileData.FileSizeString = fileData.FileSize / 1000 + 'KB'
        }
        else {
          fileData.FileSizeString = fileData.FileSize / 1000000 + ' MB';
        }
        fileData.UploadTime = fileData.UploadTime.split('.')[0].replace('T', ' ');
        metadataToReturn.get(fileType).push(fileData);
      }
    }
    console.log(metadataToReturn);
    return metadataToReturn;
  }
}
