﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;


namespace AngularWebAPIFileUploader.Models
{
    public class FileToUpload
    {
        public string User { get; set; }
        public IFormFile FileForm { get; set; }
    }

    // This metadata will be displayed to the user, grouped by file type
    public class BaseMetadata
    {
        public string User { get; set; }
        public string FileName { get; set; }        // without extension
        public string FileNameUnique { get; set; }  // without extension
        public long FileSize { get; set; }
        public DateTime UploadTime { get; set; }

        public BaseMetadata(string user, string fName, string fNameUnique, long fSize, DateTime uploadTime)
        {
            User = user;
            FileName = fName;
            FileNameUnique = fNameUnique;
            FileSize = fSize;
            UploadTime = uploadTime;
        }
    }

    public class FileMetadata : BaseMetadata
    {
        private static readonly IReadOnlyCollection<string> allowedExtensions =
            new HashSet<string>(new string[] { ".pdf", ".doc", ".png", ".docx", ".jpg" });
        private static readonly long maxSize = 500000;
        
        public string FileType { get; private set; }    // without period
        public static string[] AllowedExtensions
        {
            get
            {
                return allowedExtensions.ToArray();
            }
            private set
            {
                return;
            }
            
        }
        public static long MaxSize {
            get
            {
                return maxSize;
            }
            private set
            {
                return;
            }
        }

        public FileMetadata(string user, string fName, string fType, 
            string fNameUnique, long fSize, DateTime uploadTime) : 
            base(user, fName, fNameUnique, fSize, uploadTime)
        {
            FileType = fType;
        }

        public FileMetadata(string user, DateTime uploadTime, IFormFile file, string fNameUnique) :
            base(user, Path.GetFileNameWithoutExtension(file.FileName), fNameUnique, 
                file.Length, uploadTime)
        {
            FileType = Path.GetExtension(file.FileName);
        }

        public bool IsValidExtension()
        {
            return allowedExtensions.Contains(FileType);
        }

        public bool IsValidFileSize()
        {
            return FileSize <= maxSize && FileSize > 0;
        }

        public string JsonSerialise()
        {
            IDictionary<string, BaseMetadata[]> groupedMetadata = 
                new Dictionary<string, BaseMetadata[]>();
            groupedMetadata.Add(FileType, new BaseMetadata[] { GetBaseMetadata() });
            return JsonConvert.SerializeObject(groupedMetadata);
        }

        public BaseMetadata GetBaseMetadata()
        {
            return new BaseMetadata(User, FileName, FileNameUnique, FileSize, 
                UploadTime);
        }
}
}
