﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Net.Http.Headers;
using AngularWebAPIFileUploader.Models;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;

namespace AngularWebAPIFileUploader.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileUploadController : ControllerBase
    {
        private readonly string FILE_PATH = 
            Path.Combine(Directory.GetCurrentDirectory(), 
                "wwwroot", "Storage", "Files");
        private readonly string METADATA_PATH =
            Path.Combine(Directory.GetCurrentDirectory(), 
                "wwwroot", "Storage", "metadata.json");

        // Adds a file's metadata to the metadata JSON
        // Ideally, this is work for a database (multiple reads/writes 
        // at once, faster access, etc.), but that's overkill for something this size
        public void SaveMetadata(FileMetadata metadataToSave,
            string metadataPath)
        {
            IDictionary<string, BaseMetadata[]> allMetadata =
                new Dictionary<string, BaseMetadata[]>();

            // We have to convert to base class to avoid JSON problems
            BaseMetadata baseMetadataToSave = metadataToSave.GetBaseMetadata();

            using (StreamReader sc = System.IO.File.OpenText(metadataPath))
            {
                JsonSerializer serialiser = new JsonSerializer();
                allMetadata = (IDictionary<string, BaseMetadata[]>)serialiser.Deserialize(
                    sc, typeof(IDictionary<string, BaseMetadata[]>));
                sc.Close();
            }

            string type = metadataToSave.FileType;
            BaseMetadata[] fileSubset = new BaseMetadata[] { };

            if (allMetadata.TryGetValue(type, out fileSubset))
            {
                allMetadata[type] = fileSubset.Append(baseMetadataToSave).ToArray();
            }
            else
            {
                allMetadata.Add(type, new BaseMetadata[] { baseMetadataToSave });
            }

            using (StreamWriter sw = System.IO.File.CreateText(metadataPath))
            {
                JsonSerializer serialiser = new JsonSerializer();
                serialiser.Serialize(sw, allMetadata);
                sw.Close();
            }
        }

        // Returns true if successful, false if not
        public bool DeleteMetadata(string fileNameUnique, string fileType,
            string metadataPath)
        {
            IDictionary<string, BaseMetadata[]> allMetadata =
                new Dictionary<string, BaseMetadata[]>();

            using (StreamReader sc = System.IO.File.OpenText(metadataPath))
            {
                JsonSerializer serialiser = new JsonSerializer();
                allMetadata = (IDictionary<string, BaseMetadata[]>)
                    serialiser.Deserialize(sc,
                    typeof(IDictionary<string, BaseMetadata[]>));
            }

            BaseMetadata[] fileSet;

            // Check if the filetype is correct
           
            if (!allMetadata.TryGetValue(fileType, out fileSet))
            {
                return false;
            }
          
            IEnumerable<BaseMetadata> queryFile =
                from file in fileSet
                where file.FileNameUnique == fileNameUnique
                select file; // unique, array will only have one element

            if (!queryFile.Any())
            {
                return false;
            }

            // Need to call First since linq returned an array
            fileSet = fileSet.Where(f => f != queryFile.First()).ToArray();

            allMetadata[fileType] = fileSet;

            using (StreamWriter sw = System.IO.File.CreateText(metadataPath))
            {
                JsonSerializer serialiser = new JsonSerializer();
                serialiser.Serialize(sw, allMetadata);
            }

            return true;
        }

        // GET api/fileupload
        // Returns formatted metadata json
        [HttpGet]
        [EnableCors("AngularRequests")]
        public IActionResult Get()
        {
            try
            {
                IDictionary<string, BaseMetadata[]> metadata = new
                    Dictionary<string, BaseMetadata[]>();
                using (StreamReader sc =
                    System.IO.File.OpenText(METADATA_PATH))
                {
                    JsonSerializer serialiser = new JsonSerializer();
                    metadata = (IDictionary<string, BaseMetadata[]>)
                        serialiser.Deserialize(sc, typeof(
                        IDictionary<string, BaseMetadata[]>));
                }

                var returnObject = new { metadataJson = metadata,
                    maxSize = FileMetadata.MaxSize,
                    allowedTypes = FileMetadata.AllowedExtensions};

                string returnJson = JsonConvert.SerializeObject(returnObject);

                return Ok(returnJson);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: ${ex.ToString()}");
            }
        }

        // POST api/fileupload
        [HttpPost]
        [DisableRequestSizeLimit]       // We have our own size customisation
        [EnableCors("AngularRequests")]
        public IActionResult Upload(
            [FromForm(Name = "user")] string user)
        {
            try
            {
                IFormFile[] files = Request.Form.Files.ToArray();
                if(files.Count() != 1)
                {
                    return BadRequest("Incorrect file number");
                }
                IFormFile file = files[0];
                if (file.Length <= 0)
                {
                    return BadRequest("Invalid file");
                }
                
                // Not best practise but expanding will be beyond scale of project
                string pathToSave = FILE_PATH;
                DateTime uploadTime = DateTime.Now;
                string uploadTimeString = DateTime.Now.ToString("yyyy-MM-dd-T-HH-mm-ss");

                // Combines username, upload time and file name.
                // Vulnerable to filename attacks, but security isn't a consideration in this project
                string fileName = user + '_' + uploadTimeString + '_' +
                    ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                string fullPath = Path.Combine(pathToSave, fileName);

                FileMetadata metadata = new FileMetadata(user, uploadTime, file, 
                    Path.GetFileNameWithoutExtension(fileName));

                // Our frontend will deduce what went wrong via these error codes
                if (!metadata.IsValidExtension())
                {
                    return StatusCode(415, $"File extension (.{metadata.FileType}) invalid");
                }
                else if (!metadata.IsValidFileSize())
                {
                    return StatusCode(413, $"File too large ({metadata.FileSize} bytes)");
                }

                using (FileStream stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                    stream.Close();
                }

                SaveMetadata(metadata, METADATA_PATH);

                // Would ideally delete the file if anything errors, but beyond scope
                return Ok(metadata);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
                //return StatusCode(500, $"Internal server error: ${ex.ToString()}");
            }
        }
        
        // DELETE api/fileupload/myfile.txt
        // Deletes file with given filename
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            try
            {
                if (!id.Any())
                { 
                    return BadRequest("No filename provided");
                }

                string fileType = Path.GetExtension(id);
                string fileName = Path.GetFileNameWithoutExtension(id);
                string fullPath = Path.Combine(FILE_PATH, id);

                if (!DeleteMetadata(fileName, fileType, METADATA_PATH))
                {
                    return BadRequest($"No metadata found: {id}");
                }

                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }
                else
                {
                    return BadRequest($"File not found in storage: {id}");
                }

                return Ok($"Deleted: {id}");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex}");
            }
        }
    }
}
