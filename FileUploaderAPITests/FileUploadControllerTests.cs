using System;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Xunit;
using AngularWebAPIFileUploader.Controllers;
using AngularWebAPIFileUploader.Models;
using Newtonsoft.Json;

namespace FileUploaderAPITests
{
    public class FileUploadControllerTest
    {
        private FileUploadController _controller;
        private readonly DateTime CURRENT_TIME = DateTime.Now;

        private readonly string METADATA_PATH =
            Path.Combine(Directory.GetCurrentDirectory(),
                "Storage", "metadata.json");

        private readonly FileMetadata METADATA_MOCK_TO_ADD;

        private readonly string METADATA_MOCK_ADDED =
            @"{"".mockExt"":[{""User"": ""mockUser"",""FileName"": 
""mockName"",""FileNameUnique"": ""mockUniqueName"",""FileSize"": 100000,
""UploadTime"": ""2020-01-27T20:29:51.6816067+10:00""}]}";

        private const string METADATA_MOCK_ADD = 
            @"{}";
        private const string METADATA_MOCK_DELETE =
            @"{
                "".mockExt"": [
                    {
                        ""User"": ""mockUser"",
                        ""FileName"": ""mockName"",
                        ""FileNameUnique"": ""mockUser_2020-01-27-T-20-29-51_mockName"",
                        ""FileSize"": 100000,
                        ""UploadTime"": ""2020-01-27T20:29:51.6816067+10:00""
                    }
                ]
            }";

        public FileUploadControllerTest()
        {
            _controller = new FileUploadController();

            METADATA_MOCK_TO_ADD =
            new FileMetadata("mockUser", "mockName", ".mockExt",
                "mockUniqueName", 100000, CURRENT_TIME);
        }

        [Fact]
        public void Delete_NoIdPassed_ReturnsBadRequest()
        {
            // Act
            IActionResult result = _controller.Delete(id: "invalid_file");
            BadRequestResult badResult = result as BadRequestResult;

            // Assert
            Assert.NotNull(badResult);
            Assert.Equal(400, badResult.StatusCode);
        }

        [Fact]
        public void Get_WhenCalled_ReturnsOkRequest()
        {
            // Act
            IActionResult result = _controller.Get();
            OkResult okResult = result as OkResult;

            // Assert
            Assert.NotNull(okResult);
            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public void SaveMetadata_OnSave_SavesCorrectObject()
        {
            // Assemble
            
            using (StreamWriter fs = File.CreateText(METADATA_PATH))
            {
                fs.Write(METADATA_MOCK_ADD);
            }
            string serialisedMockMetadata = METADATA_MOCK_TO_ADD.JsonSerialise();

            // Act

            _controller.SaveMetadata(METADATA_MOCK_TO_ADD, METADATA_PATH);
            string serialisedSavedMetadata = File.ReadAllText(METADATA_PATH);

            // Assert
        }
    }
}
